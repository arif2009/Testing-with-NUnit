Different functionality for .net NUnit test

**AssertDemo project contains :**

2.1 Introduction

2.2 Asserting String Equality

2.3 Asserting Numerical Equality

2.4 Asserting DateTime Equality

2.5 Asserting with Ranges

2.6 Assert Nulls and Booleans

2.7 Asserting on Collections?

2.8 Asserting Reference Equality

2.9 Asserting on Object Types and Properties

2.10 Asserting Exceptions Are Thrown

2.11 Summary

**AttributesDemo project contains :**

3.1 Introduction

3.2 Running Code Before and After Each Test

3.3 Running Code Before and After a Fixture

3.4 Running Code Before and After Assemblies and Namespaces

3.5 Running a Test with Multiple Test Case Data

3.6 Reusing Test Case Data

3.7 Generating Combinations of Input Data

3.8 Temporarily Ignoring Tests

3.9 Organizing Tests into Categories

3.10 Specifying a Maximum Test Run Time

3.11 Repeating a Test Multiple Times

3.12 Summary